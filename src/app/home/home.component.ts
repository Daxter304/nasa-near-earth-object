import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DateTime } from 'luxon';
import staticNeoData from '../data/staticNeoData';

// Define the response from the API
interface NasaNeoDataInterface {
  element_count: Number,
  links: Object,
  near_earth_objects: Object
}

interface NeoStatsData {
  closest: {
    distance: Number,
    name: String,
    flyByDate: String,
    speed: Number
  },
  biggest: {
    diameter: Number,
    name: String,
    flyByDate: String,
    speed: Number
  }
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  private loading: Boolean;
  private nearEarthObjectList: Object;
  private neoDates: Array<string>;
  private neoStats: NeoStatsData;

  // The API used has a rate limit for the demo API key
  private overRateLimit = false;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.loading = true;

    // Get the start and end date for the API call
    const today = DateTime.local();
    const startDate = today.minus({
      day: 7
    }).toISODate();
    const endDate = today.toISODate();

    // API Doc: https://api.nasa.gov/
    // Create the Near Earth Object URL
    const nasaNeoApiUrl = `https://api.nasa.gov/neo/rest/v1/feed?start_date=${startDate}&end_date=${endDate}&api_key=DEMO_KEY`;

    // Make a GET call to the NASA Web Services API
    this.http.get(nasaNeoApiUrl).subscribe(
      (response: NasaNeoDataInterface) => {
        this.compileNeoData(response);
        this.loading = false;
      },
      errorResponse => {
        if (errorResponse.error.error.code === 'OVER_RATE_LIMIT') {
          console.error(errorResponse);
          this.overRateLimit = true;
          this.compileNeoData(staticNeoData);
          this.loading = false;
        } else {
          alert(errorResponse.error.error.message);
        }
      });
  }

  private compileNeoData(neoData: NasaNeoDataInterface) {
    console.log(neoData);

    const neoDates = Object.keys(neoData.near_earth_objects);

    // Sort dates by most recent
    this.neoDates = neoDates.sort(function (a: any, b: any) {
      a = new Date(a);
      b = new Date(b);
      return a > b ? -1 : a < b ? 1 : 0;
    });

    const nearEarthObjectList = this.nearEarthObjectList = neoData.near_earth_objects;

    const neoStats: NeoStatsData = {
      closest: {
        distance: -1,
        name: '',
        flyByDate: '',
        speed: 0
      },
      biggest: {
        diameter: -1,
        name: '',
        flyByDate: '',
        speed: 0
      }
    }

    // Calculate some stats about near Earth objects
    for(const date of this.neoDates) {
      const neoData = nearEarthObjectList[date];
      for (const object of neoData) {
        const missDistance = Math.round(object.close_approach_data[0].miss_distance.miles);
        const minSize = object.estimated_diameter.feet.estimated_diameter_min;
        const maxSize = object.estimated_diameter.feet.estimated_diameter_max;
        const averagedSize = (minSize + maxSize) / 2;
        const flyByDate = object.close_approach_data[0].close_approach_date;
        const speed = Math.round(object.close_approach_data[0].relative_velocity.miles_per_hour);

        if (missDistance < neoStats.closest.distance || neoStats.closest.distance === -1) {
          neoStats.closest.distance = missDistance;
          neoStats.closest.name = object.name;
          neoStats.closest.flyByDate = flyByDate;
          neoStats.closest.speed = speed;
        }

        if (averagedSize > neoStats.biggest.diameter || neoStats.biggest.diameter === -1) {
          neoStats.biggest.diameter = averagedSize;
          neoStats.biggest.name = object.name;
          neoStats.biggest.flyByDate = flyByDate;
          neoStats.biggest.speed = speed;
        }
      }
    }

    this.neoStats = neoStats;
  }

  private roundNumber(number) {
    return Math.round(number);
  }
}